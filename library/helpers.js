/**
 *
 * @param {string} category
 */
function getTopLevelCategory(category) {
  return category.split(':')[0]
}

/**
 *
 * @param {array} events Array of events
 */
function groupEventsByCategory(events) {
  const categories = {}
  for (const event of events) {
    const category = getTopLevelCategory(event.category || '')
    if (categories[category] === undefined) {
      categories[category] = []
    }
    categories[category].push(event)
  }
  const ordered = {}
  Object.keys(categories)
    .reverse()
    .forEach((key) => {
      ordered[key] = categories[key]
    })
  return ordered
}

function makeQrData(short_id, name, college, events, mac) {
  return short_id
}

export default {
  makeQrData,
  groupEventsByCategory
}
