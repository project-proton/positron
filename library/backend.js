/* eslint-disable camelcase */
/* eslint require-await: 0 */
import ky from 'ky'

const API_ROOT = 'https://api.tathva.org'
const OFFLINE_QR_API_ROOT = API_ROOT + '/participants/self/qr'
async function register({
  name,
  gender,
  college,
  email,
  mobile,
  userid,
  referral_code,
  password
}) {
  const participant = {
    name,
    gender,
    college,
    email,
    mobile,
    username: userid,
    password,
    referral_code
  }
  try {
    await ky.post(API_ROOT + '/auth/participant/register', {
      json: participant
    })
    return { status: 'OK' }
  } catch (e) {
    if (!e.response) throw e
    switch (e.response.status) {
      case 400:
        return {
          status: 'ERR',
          code: 'E_BAD_REQUEST',
          body: await e.response.text()
        }
      case 500:
        return { status: 'ERR', code: 'E_USER_EXISTS' }
      default:
        throw e
    }
  }
}

async function getCertificates(token) {
  try {
    const url = API_ROOT + '/participants/self/certs'
    let res = await ky.get(url, {
      headers: { Authorization: 'Bearer ' + token }
    })
    res = (await res.json()).participant.certificates
    return res
  } catch (e) {
    throw e
  }
}

async function login({ userid, password }) {
  try {
    const res = await ky.post(API_ROOT + '/auth/participant/get-token', {
      json: { username: userid, password }
    })
    return { status: 'OK', token: (await res.json()).token }
  } catch (e) {
    console.log(e)
    if (!e.response) throw e
    switch (e.response.status) {
      case 401: {
        return { status: 'ERR', code: 'E_LOGIN_FAILED' }
      }
      case 403: {
        return { status: 'ERR', code: 'E_EMAIL_NOT_VERIFIED' }
      }
      case 404: {
        return { status: 'ERR', code: 'E_NO_USER' }
      }
      default:
        throw e
    }
  }
}

async function getEvents() {
  try {
    const res = await ky.get(API_ROOT + '/events')
    const events = await res.json()
    return { status: 'OK', events }
  } catch (e) {
    if (!e.response) throw e
    throw e
  }
}

async function getInterestedItems(token) {
  const url = API_ROOT + '/participants/self/interested'
  console.log(url)
  const res = await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
  console.log(res.body)
  return (await res.json()).interested
}
async function getBookedItems(token) {
  const url = API_ROOT + '/participants/self/events'
  console.log(url)
  const res = await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
  console.log(res.body)
  return (await res.json()).events
}

async function updateParticipantProfile(token, patches) {
  try {
    const res = await ky.put(API_ROOT + '/participants/self', {
      headers: { Authorization: 'Bearer ' + token },
      json: patches
    })
    if (res.statusCode !== 200);
  } catch (e) {
    throw e
  }
}

async function markLectureInterest(token, status) {
  const url = API_ROOT + '/participants/self/lecture_interest?status=' + status
  await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
}

async function markAdizyaInterest(token, status) {
  const url = API_ROOT + '/participants/self/adizya_interest?status=' + status
  await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
}

async function registerEvents(token, eventIds) {
  try {
    const res = await ky.post(API_ROOT + '/payments/checkout', {
      headers: { Authorization: 'Bearer ' + token },
      json: { events: eventIds }
    })
    const body = await res.json()
    return {
      status: 'OK',
      redirect_url: body.payment_request_url
    }
  } catch (e) {
    if (!e.response) throw e
    switch (e.response.status) {
      case 401:
        return { status: 'ERR', code: 'E_TOKEN_EXPIRED' }
      case 422:
        return { status: 'ERR', code: 'E_NO_STOCK' }
      case 500:
        return { status: 'ERR', code: 'E_THIRD_PARTY' }
      case 503:
        return { status: 'ERR', code: 'E_HOURLY_LIMIT' }
      case 505:
        return { status: 'ERR', code: 'E_TEMP_MAINT' }
      default:
        throw e
    }
  }
}

async function completeDeclaration(token) {
  const url = API_ROOT + '/participants/self/sign_declaration'
  await ky.get(url, {
    headers: { Authorization: 'Bearer ' + token }
  })
}
async function updateInterest(token, eventId, isInterested) {
  try {
    const url =
      API_ROOT + '/events/' + eventId + '/interest?status=' + isInterested
    await ky.get(url, {
      headers: { Authorization: 'Bearer ' + token }
    })
  } catch (e) {
    switch (e.response.status) {
      case 401:
        throw new Error('NOAUTH')
      case 400: {
        throw e
      }
      default:
        throw e
    }
  }
}

async function getParticipantProfile(token) {
  try {
    const res = await ky.get(API_ROOT + '/participants/self', {
      headers: { Authorization: 'Bearer ' + token }
    })
    return { status: 'OK', participant: (await res.json()).participant }
  } catch (e) {
    console.log(e)
    if (e.response && e.response.status === 401)
      return { status: 'ERR', code: 'E_TOKEN_EXPIRED' }
    throw e
  }
}

async function sendPasswordResetMail(userid) {
  try {
    const url = API_ROOT + '/auth/sendreset'
    await ky.post(url, {
      json: { authID: userid }
    })
  } catch (e) {
    throw e
  }
}

async function resetPassword(body) {
  const url = API_ROOT + '/auth/resetpass'
  await ky.post(url, {
    json: body
  })
}

async function returnOfflineQR(token) {
  try {
    const url = OFFLINE_QR_API_ROOT
    const qr = (await (await ky.get(url, {
      headers: { Authorization: 'Bearer ' + token }
    })).json()).qr
    console.log(qr)
    return qr
  } catch (e) {
    throw e
  }
}

export default {
  register,
  login,
  getEvents,
  registerEvents,
  getParticipantProfile,
  updateInterest,
  updateParticipantProfile,
  completeDeclaration,
  getInterestedItems,
  getBookedItems,
  markLectureInterest,
  markAdizyaInterest,
  sendPasswordResetMail,
  resetPassword,
  returnOfflineQR,
  getCertificates
}
