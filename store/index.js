import _ from 'lodash'
import { backend } from '~/library/main'
import helpers from '~/library/helpers'

const VuexPluginLocalStorage = (store) => {
  let dataStored = localStorage.getItem('proton-state')
  if (dataStored) {
    dataStored = JSON.parse(dataStored)
  }
  if (
    dataStored &&
    dataStored.VERSION &&
    dataStored.VERSION === LOCAL_STORAGE_DATA_VERSION
  ) {
    store.commit('__load__', dataStored)
  } else {
    localStorage.removeItem('proton-state')
  }

  store.subscribe((mutation, state) => {
    localStorage.setItem('proton-state', JSON.stringify(state))
  })
}

export const plugins = [VuexPluginLocalStorage]

const LOCAL_STORAGE_DATA_VERSION = 4
export const state = () => ({
  VERSION: LOCAL_STORAGE_DATA_VERSION,

  isLoggedIn: false,
  loggedInToken: null,

  participantProfile: null,
  profileLoaded: false,
  booked_items: [],
  interested_items: [],
  offline_qr: null,
  certificates: [],

  events: [],
  eventsByCategory: {},
  eventsLoaded: false,

  cart: []
})

export const mutations = {
  __load__(state, stateNew) {
    const stateNewFiltered = _.pick(stateNew, [
      'isLoggedIn',
      'loggedInToken',
      'cart'
    ])
    Object.assign(state, stateNewFiltered)
  },
  setCertificates(state, certificates) {
    state.certificates = certificates
  },
  setLoggedIn(state, token) {
    state.loggedInToken = token
    state.isLoggedIn = true
  },
  setLoggedOut(state) {
    state.loggedInToken = null
    state.isLoggedIn = false
    state.participantProfile = null
  },
  setProfileNeedsReload(state) {
    state.profileLoaded = false
  },
  setProfile(state, profile) {
    state.participantProfile = profile
    state.profileLoaded = true
  },
  setEventsNeedReload(state) {
    state.eventsLoaded = false
  },
  setEvents(state, events) {
    state.events = events
    state.eventsLoaded = true
  },
  setEventsByCategory(state, eventsByCategory) {
    state.eventsByCategory = eventsByCategory
  },
  setBookedItems(state, events) {
    state.booked_items = events
  },
  setInterestedItems(state, events) {
    state.interested_items = events
  },
  pushToCart(state, event) {
    if (state.cart.find((e) => e._id === event._id) !== undefined) {
      // do not add if already in cart
      return
    }
    state.cart.push(event)
  },
  removeFromCart(state, event) {
    state.cart = state.cart.filter((e) => e._id !== event._id)
  },
  fixCart(state) {
    if (!state.participantProfile) return
    state.cart = state.cart.filter(
      (e) => !state.participantProfile.events.includes(e._id)
    )
  },
  setOfflineQr(state, offline_qr) {
    state.offline_qr = offline_qr
  }
}

export const actions = {
  async login(context, loginData) {
    try {
      const resp = await backend.login(loginData)
      if (resp.status === 'OK') {
        context.commit('setLoggedIn', resp.token)
        await context.dispatch('loadProfile')
      }
      return resp
    } catch (e) {
      throw e
    }
  },
  logout(context) {
    context.commit('setLoggedOut')
  },
  async loadProfile(context) {
    const token = context.state.loggedInToken
    if (!token) return
    const resp = await backend.getParticipantProfile(token)
    if (resp.code === 'E_TOKEN_EXPIRED') {
      await context.dispatch('logout')
      return
    }
    const { participant } = resp
    participant.qrData = helpers.makeQrData(
      participant.short_id,
      participant.name,
      participant.college,
      participant.events
    )
    context.commit('setProfile', participant)
    context.commit('fixCart')
  },
  async loadBookedItems(context) {
    const token = context.state.loggedInToken
    if (!token) return
    const items = await backend.getBookedItems(token)
    context.commit('setBookedItems', items)
  },
  async loadInterestedItems(context) {
    const token = context.state.loggedInToken
    if (!token) return
    const items = await backend.getInterestedItems(token)
    context.commit('setInterestedItems', items)
  },
  async loadData(context) {
    const { events } = await backend.getEvents()
    const eventsByCategory = helpers.groupEventsByCategory(events)
    context.commit('setEvents', events)
    context.commit('setEventsByCategory', eventsByCategory)
  },
  async saveProfileEdits(context, edits) {
    console.log(edits)
    try {
      await backend.updateParticipantProfile(context.state.loggedInToken, edits)
      return true
    } catch (e) {
      if (e.response && e.response.status === 401) {
        await context.dispatch('logout')
      } else throw e
    }
  },
  async markLectureInterest(context, status) {
    const token = context.state.loggedInToken
    await backend.markLectureInterest(token, status)
    await context.dispatch('loadProfile')
  },
  async markAdizyaInterest(context, status) {
    const token = context.state.loggedInToken
    await backend.markAdizyaInterest(token, status)
    await context.dispatch('loadProfile')
  },
  addToCart(context, event) {
    context.commit('pushToCart', event)
  },
  async addToInterest(context, event) {
    try {
      await backend.updateInterest(
        context.state.loggedInToken,
        event._id,
        'true'
      )
    } catch (e) {
      throw e
    }
  },
  removeFromCart(context, event) {
    context.commit('removeFromCart', event)
  },
  async checkoutCart(context) {
    const token = context.state.loggedInToken
    if (!token) return false
    context.commit('setProfileNeedsReload')
    context.commit('setEventsNeedReload')

    const eventIds = context.state.cart.map((e) => e._id)
    const resp = await backend.registerEvents(token, eventIds)
    if (resp.code === 'E_TOKEN_EXPIRED') {
      await context.dispatch('logout')
    }
    return resp
  },
  async completeDeclaration(context) {
    await backend.completeDeclaration(context.state.loggedInToken)
  },
  async sendPasswordResetMail(context, userid) {
    await backend.sendPasswordResetMail(userid)
  },
  async resetPassword(context, body) {
    await backend.resetPassword(body)
  },
  async removeFromInterested(context, event_id) {
    await backend.updateInterest(context.state.loggedInToken, event_id, 'false')
    const modified_interests = context.state.interested_items.slice(0)
    console.log(
      'Index',
      modified_interests.findIndex((item) => item._id === event_id)
    )
    modified_interests.splice(
      modified_interests.findIndex((item) => item._id === event_id),
      1
    )
    context.commit('setInterestedItems', modified_interests)
  },

  async loadCertificates(context) {
    try {
      const certificates = await backend.getCertificates(
        context.state.loggedInToken
      )
      context.commit('setCertificates', certificates)
    } catch (e) {
      throw e
    }
  },
  async fetchOfflineQR(context) {
    try {
      const offline_qr = await backend.returnOfflineQR(
        context.state.loggedInToken
      )
      context.commit('setOfflineQr', offline_qr)
    } catch (e) {
      if (e.response && e.response.status === 403) throw new Error('NOPASS')
      throw e
    }
  }
}
