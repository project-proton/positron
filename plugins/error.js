import Vue from 'vue'

Vue.config.errorHandler = function(err, vm, info) {
  // handle error
  // `info` is a Vue-specific error info, e.g. which lifecycle hook
  // the error was found in. Only available in 2.2.0+
  if (err.response && err.response.status === 401) {
    window.location.href = '/logout'
    return
  }
  if (err.response && err.response.message === 'jwt malformed') {
    window.location.href = '/logout'
  }
}
