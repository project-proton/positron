export default function({ store, route, redirect }) {
  if (
    route.meta[0] &&
    ((route.meta[0].routeOnlyWhen === 'LOGGED_OUT' && store.state.isLoggedIn) ||
      (route.meta[0].routeOnlyWhen === 'LOGGED_IN' && !store.state.isLoggedIn))
  ) {
    redirect('/')
  }
}
